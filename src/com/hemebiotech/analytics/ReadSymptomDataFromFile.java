package com.hemebiotech.analytics;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Simple brute force implementation
 *
 */
public class ReadSymptomDataFromFile implements ISymptomReader {

	private String filepath;
	/**
	 	* @param filepath a full or partial path to file with symptom strings in it, one per line
	 */
	public ReadSymptomDataFromFile (String filepath)
	{
		this.filepath = filepath;
	}

	public ReadSymptomDataFromFile() {

	}

	@Override
	/**
		* the GetSymptoms method allows you to determine the number of occurrences
		* of symptoms from the symtom.txt file as long as the line is not null
	*/

	public List<String> GetSymptoms() {
		List<String> result = new ArrayList<>();
		Map<String, Integer> occurence = new TreeMap<>();

		if (this.filepath != null) {
			try {

				BufferedReader reader = new BufferedReader (new FileReader(filepath));

				String line = reader.readLine();
				while (line!= null) {
					if (occurence.containsKey(line)) {
						occurence.put(line, occurence.get(line) + 1);
					}
					else {
						occurence.put(line, 1);
					}
				//Concatenation of the key and the value to store it in the result list

					result = occurence.entrySet()
							.stream()
							.map(entry -> entry.getKey() + " = " + entry.getValue() + " OCCURENCE(s) ")
							.sorted()
							.collect(Collectors.toList());
					line = reader.readLine();
				}
				//this command is used to close the file
				reader.close();
			} catch (IOException e) {
				System.out.println(" error file.");
				e.printStackTrace();
			}
		}
		
		return result;
	}

	/*public void GetSymptomsWritter(List<String> result, String file) throws IOException {
		try {
			FileWriter writer = new FileWriter (file);
			for(String element: result) {

				writer.write(element + System.lineSeparator());
			}
			// close the file
			writer.close();
		}
        catch (Exception e) {
		e.getStackTrace();
	}


	}*/
}

