package com.hemebiotech.analytics;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class WriterSymptomDataFile {
    public static void GetSymptomsWritter(List<String> result, String file) throws IOException {
        /**
         *
         * @param file is the full or partial path to store the symtoms with their number of occurrences
         * @param result Is the list containing the Symptoms. The list is in character string
         */
        try {
            FileWriter writer = new FileWriter (file);
            for(String element: result) {

                writer.write(element + System.lineSeparator());
            }
            // this command is used to close the file
            writer.close();
        }
        catch (Exception e) {
            e.getStackTrace();
        }


    }
}
