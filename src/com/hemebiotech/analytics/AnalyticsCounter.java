package com.hemebiotech.analytics;


import java.io.*;
import java.util.*;

public class AnalyticsCounter {

	public AnalyticsCounter() throws IOException {}
	public static void main(String args[]) throws Exception {
			/*filepath indicates the path to the symptom file*/
			// first get input
		String filepath = "symptoms.txt";
		String newfilepath = "result.out";
		//Reads File
		ReadSymptomDataFromFile readSymptomDataFromFile = new ReadSymptomDataFromFile(filepath);
		System.out.println(readSymptomDataFromFile.GetSymptoms());
		// Writter newfilepath
		List<String> result= readSymptomDataFromFile.GetSymptoms();
		WriterSymptomDataFile.GetSymptomsWritter(result,newfilepath);

	}

}