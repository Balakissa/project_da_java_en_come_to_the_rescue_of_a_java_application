package com.hemebiotech.analytics;

import java.io.IOException;
import java.util.List;

public interface ISymptomWriter {
    /**
     *
     * @param file is the full or partial path to store the symtoms with their number of occurrences
     * @param result Is the list containing the Symptoms. The list is in character string
     * @return do not return anything
     */
    public void  GetSymptomsWritter(List<String> result, String file) throws IOException;
}
